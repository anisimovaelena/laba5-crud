<?php
session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>


<?php require_once('header.php'); 

?>
<div class="container">
    <div class="row col-10">
        <table class="table thead-light">
            <tr >
            <th scope="col" >Id</th>
            <th scope="col" >Имя</th>
            <th scope="col" >Январь</th>
            <th scope="col" >Февраль</th>
            <th scope="col" >Март</th>
            <!-- <th scope="col" >Кол-во пропусков в месяце</.th> -->
            
            </tr>
            <?php

                    /* Делаем выборку всех строк из таблицы "payment" */

                    $payment=$link->prepare("SELECT * FROM payment INNER JOIN general ON payment.id_name = general.id ORDER BY payment.id_pay");
                    $payment->execute();


                    /* Преобразовываем полученные данные в нормальный массив */

                    $payment=$payment->fetchAll();

                    /*
                    * Перебираем массив и рендерим HTML с данными из массива
                    * Ключ 0 - id
                    * Ключ 1 - name
                    * Ключ 2 - january
                    * Ключ 3 - february
                    * Ключ 4 - march
                    
                    */

                    foreach ($payment as $payment) {
                        ?>
                            <tr>
                                <td scope="row" ><?= $payment[0] ?></td>
                                <td><?= $payment[1] ?></td>
                                <td><?= $payment[2] ?></td>
                                <td><?= $payment[3] ?></td>
                                <td><?= $payment[4] ?></td>
                                <td><?= $payment[5] ?></td>
                            </tr>
                        <?php
                    } 
                ?>
        </table>
    </div>
    <div class="row">
            <a class="btn btn-info" href="create-form.php" role="button"> Добавить </a>
            
    </div>
</div>

<div class="modal fade" id="infoKidForm" tabindex="-1" role="dialog" aria-labelledby="infoKidFormLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="infoKidFormLabel">Платёж для ребёнка:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container">
        <div class="row">
            <img src=".." alt="..." class="rounded float-left" width="200"><br>
            <ul class="list-group list-group-flush">
            <li class="list-group-item"><h3 class="name"></h3></li>
            <li class="parent list-group-item"></li>
            <li class="birthday list-group-item"></li>
            <li class="num_group list-group-item"></li>
            <li class="pass list-group-item"></li>
            </ul>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>
<br>
<br>
    <div>
        <a class="btn btn-warning" href="logout.php"> Выход </a>
    </div>



<?php require_once('footer.php');?>

</body>
</html>