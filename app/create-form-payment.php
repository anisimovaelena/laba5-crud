<?php session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
$kid=$link->prepare("SELECT `id`, `name` FROM `general`");
$kid->execute();
$kid=$kid->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>
<?php require_once('header.php'); 
require_once('connect.php');
?>
<div class="container">
<!-- Форма добавления -->
    <form >
        <div class="form-inline">
            <div class="form-group">
                <div class="col-4">
                    <label for="name">Ребёнок</label>
                    <select id="select" class="custom-select">
                        <?php
                            foreach ($kid as $kid) {
                                ?>
                                    <option value="<?=$kid['id']?>"><?=$kid['name']?></option>
                                <?php
                            } 
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-3">
                <label for="num_group">Сумма</label>
                <input class="form-control" type="number" name="money"> </div>
            </div>
            <div class="form-group ">
            <div class="col-3">
                <label for="birthday">Дата</label>
                <input class="form-control" type="date" name="date"> </div>
            </div>
            <div class="form-group ">
                <div class="col-2">
                    <button type="submit" class="create-pay-btn btn btn-primary">Добавить оплату</button>
                </div>
            </div>
        </div>
    </form>
</div>
<br><br>
<?php require_once('footer.php');?>

<script src="../js/jquery-3.4.1.min.js"></script>
<script src="../js/create.js"></script>



</body>
</html>