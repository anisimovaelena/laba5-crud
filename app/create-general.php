<?php
require_once 'connect.php';
session_start();
error_reporting(E_ERROR);

if($_SERVER["REQUEST_METHOD"]== "POST") {
        $id= $_POST['id'];
        $name = $_POST['name']; 
        $parent=$_POST['parent'];
        $birthday = $_POST['birthday'];
        $num_group = $_POST['num_group'];
        $pass = $_POST['pass'];
    
    define("UPLOAD_DIR", "../uploads/");
    if (!empty($_FILES["avatar"])) {
        $avatar = $_FILES["avatar"];

        if ($avatar["error"] !== UPLOAD_ERR_OK) {
            echo "<p>Произошла ошибка.</p>";
            exit;
        }

        // обеспечиваем безопасное имени файла
        $name_file = preg_replace("/[^A-Z0-9._-]/i", "_", $avatar["name"]);

        // не перезаписываем существующий файл
        $i = 0;
        $parts = pathinfo($name_file);
        while (file_exists(UPLOAD_DIR . $name_file)) {
            $i++;
            $name_file = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // сохраняем файл из временного каталога
        $success = move_uploaded_file($avatar["tmp_name"],
            UPLOAD_DIR . $name_file);
        if (!$success) { 
            echo "<p>Не удалось сохранить файл.</p>";
            exit;
        }

        // устанавливаем правильные права для нового файла
        chmod(UPLOAD_DIR . $name_file, 0644);
        $path=UPLOAD_DIR.$name_file;
    }

    $create_general=$link->prepare('INSERT INTO `general` (`name`, `parent`, `birthday`, `num_group`, `pass`, `avatar`) VALUES ( ?, ?, ?, ?, ?, ?)');
    $create_general->execute([$name, $parent, $birthday, $num_group, $pass, $path]);
    
    if ($create_general && !is_string($create_general)) {
        $response = [
            "status" => true
        ];
    }
    else $response = [
        "status" => false
    ];
    echo json_encode($response); // чтобы преобразовать php массив в json  
}

