<?php session_start();

if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
} 
$kid_id=$_GET['id'];
require_once('connect.php');
$kid=$link->prepare("SELECT * FROM `general` WHERE `id` = '$kid_id'");
$kid->execute();
$kid=$kid->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>
<?php require_once('header.php'); 

?>
<div class="container">
<!-- Форма изменения -->
    <form >
            <div class="form-group row"> <input type="hidden" name="id" value="<?= $kid_id ?>"> </div>
            
            <div class="form-group row">
                <div class="col-5">
                <label for="name">Имя</label>
                <input type="text" class="form-control" name="name" value="<?=$kid[0]['name']?>"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                    <label for="parent">Родитель</label>
                    <input class="form-control" type="name" name="parent" value="<?=$kid[0]['parent']?>" > </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                <label for="birthday">Дата рождения</label>
                <input class="form-control" type="date" name="birthday" value="<?=$kid[0]['birthday']?>"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                <label for="num_group">Номер группы</label>
                <input class="form-control" type="number" name="num_group" value="<?=$kid[0]['num_group']?>"> </div>
            </div>
            <div class="form-group row">
                <div class="col-5">
                <label for="pass">Кол-во пропусков в месяце</label>
                <input class="form-control" type="number" name="pass" value="<?=$kid[0]['pass']?>"> </div>
            </div>

            <div class="form-group row">
                <div class="col-5">
                    <button type="submit" class="update-btn btn btn-primary">Изменить</button>
                </div>
            </div>

            <label for="avatar">Фото</label>
                        <div class="form-check ">
                        <input class="form-check-input" type="checkbox" value="" id="delete_checkbox">
                            <label class="form-check-label" for="">
                                Удалить фото
                            </label>
                        </div>
                        <div class="form-group row">
                            <div class="col-5">
                            <img id="img-to-edit" scr="<?=$kid[0]['avatar']?>" class="img-fluid" style="cursor:pointer" title="изменить аватар">
                            <input class="form-control-file" type="file" id="update_avatar" name="update_avatar"  > </div>
                        </div> 
        </form>
</div>
<?php require_once('footer.php');?>

<script src="../js/jquery-3.4.1.min.js"></script>
<script src="../js/update.js"></script>



</body>
</html>