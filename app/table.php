<?php
session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>


<?php require_once('header.php'); 

?>
<div class="container">
    <div class="row col-12">
        <table class="table thead-light">
            <tr >
            <th scope="col" >Номер</th>
            <th scope="col" >Имя</th>
            <th scope="col" >Родитель</th>
            <th scope="col" >Дата рождения</th>
            <th scope="col" >Номер группы</th>
            <th scope="col" >Кол-во пропусков в месяце</th>
            <th scope="col" >Фото</th>
            </tr>
            <?php
                    /* Делаем выборку всех строк из таблицы "general" */
                    $general = $link->prepare( "SELECT * FROM `general`");
                    $general->execute();
                    $general=$general->fetchAll();
                    /*
                    * Перебираем массив и рендерим HTML с данными из массива
                    * Ключ 0 - id
                    * Ключ 1 - name
                    * Ключ 2 - parent
                    * Ключ 3 - birthday
                    * Ключ 4 - num_group
                    * Ключ 5 - pass
                    */

                    foreach ($general as $general) {
                        ?>
                            <tr>
                                <td scope="row" ><?= $general['id'] ?></td>
                                <td><?= $general['name'] ?></td>
                                <td><?= $general['parent'] ?></td>
                                <td><?= $general['birthday'] ?></td>
                                <td><?= $general['num_group'] ?></td>
                                <td><?= $general['pass'] ?></td>
                                <td><img src="<?= $general['avatar'] ?>"  class="img-fluid"></td>
                                <td><a href="upd-form-general.php?id=<?= $general['id'] ?>" class="update-btn btn btn-primary"  ?> Изменить</a></td>
                                <td><a href="delete-general.php?id=<?= $general['id'] ?>" class="del-btn btn btn-primary"  ?> Удалить</a></td>
                            </tr>
                        <?php
                    } 
                ?>
        </table>
    </div>
    <div class="row">
            <a class="btn btn-info" href="create-form.php" role="button"> Добавить </a>
            
    </div>
</div>
<div class="container">
    <div class="row col-12">
    <h2>Платежи</h2>
        <table class="table thead-light">
            <tr >
            <th scope="col" >Номер</th>
            <th scope="col-2" >Имя</th>
            <th scope="col-2" >Сумма</th>
            <th scope="col-3" >Дата </th>
            
            
            </tr>
            <?php
                    /* Делаем выборку всех строк из таблицы "general" */
                    $payment = $link->prepare( "SELECT * FROM `payment`");
                    $payment->execute();
                    /*
                    * Перебираем массив и рендерим HTML с данными из массива
                    * Ключ 0 - id
                    * Ключ 1 - id_name
                    * Ключ 2 - money
                    * Ключ 3 - date
                    */

                    foreach ($payment as $payment) {
                        ?>
                            <tr>
                                <td scope="row" ><?= $payment[0] ?></td>
                                <td><a href="#"><?= $payment[1] ?></a></td>
                                <td><?= $payment[2] ?></td>
                                <td><?= $payment[3] ?></td>
                                <td><a href="upd-form-payment.php?id=<?= $payment[0] ?>" class="btn btn-primary"  > Изменить</a></td>
                                <td><a href="delete-pay.php?id=<?= $payment[0] ?>" class="del-pay-btn btn btn-primary"  > Удалить</a></td>
                            </tr>
                        <?php
                    } 
                ?>
        </table>
    </div>
    <div class="row">
            <a class="btn btn-info" href="create-form-payment.php" role="button"> Добавить </a>
            
    </div>
</div>

<br>
<br>
    <div>
        <a class="btn btn-warning" href="logout.php"> Выход </a>
    </div>



<?php require_once('footer.php');?>

</body>
</html>