<?php session_start();

if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
} 
$id_pay=$_POST['id'];
require_once('connect.php');
/*Получаем инфу о платеже */
$kid_pay=$link->prepare("SELECT * FROM `payment` WHERE `id_pay` = '$id_pay'");
$kid_pay->execute();
$kid_pay=$kid_pay->fetchAll();
$kid_pay_id=$kid_pay[0][1];
/*Получаем всех детей не связанных с этим платежом */
$kid=$link->prepare("SELECT `id`, `name`  FROM `general`");
$kid->execute();
$kid=$kid->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>
<?php require_once('header.php'); 

?>
<div class="container">
<!-- Форма изменения -->
    <form >
        <div class="form-inline"> <input type="hidden" name="id" value="<?= $id_pay ?>"> 

            <div class="form-group">
                <div class="col-4">
                    <label for="name">Ребёнок</label>
                    <select id="select" class="custom-select">
                        <?php
                            foreach ($kid as $kid) {
                                if($kid['id']==$kid_pay[0][1]) {
                        ?>
                                <option selected value="<?=$kid['id']?>"> <?=$kid['name']?> </option> <?php continue;} ?>
                                <option value="<?=$kid['id']?>"><?=$kid['name']?></option>
                            <?php
                            } 
                            ?>
                        
                    </select>
                </div>
            </div>    
            <div class="form-group">
                <div class="col-3">
                    <label for="money">Сумма</label>
                    <input class="form-control" type="name" name="money" value="<?=$kid_pay[0][2]?>" > </div>
            </div>
            <div class="form-group">
                <div class="col-3">
                <label for="birthday">Дата</label>
                <input class="form-control" type="date" name="date" value="<?=$kid_pay[0][3]?>"> </div>
            </div>
            <div class="form-group">
                <div class="col-2">
                    <button type="submit" class="update-pay-btn btn btn-primary ">Изменить</button>
                </div>
            </div>
        </div>
    </form>
    <br><br>
</div>
<?php require_once('footer.php');?>

<script src="../js/jquery-3.4.1.min.js"></script>
<script src="../js/update.js"></script>



</body>
</html>