/* 
    Валидация для формы добавления в базу данных 
*/
let avatar = false;

$('input[name="avatar"]').change(function (e) {
    avatar = e.target.files[0];
});

$('.create-btn').click(function(e) {
    e.preventDefault(); 
   
    let name = $('input[name="name"]').val();
    let parent = $('input[name="parent"]').val();
    let birthday = $('input[name="birthday"]').val();
    let num_group = $('input[name="num_group"]').val();
    let pass = $('input[name="pass"]').val();
    
    let formData = new FormData();
    formData.append('name', name);
    formData.append('parent', parent);
    formData.append('birthday', birthday);
    formData.append('num_group', num_group);
    formData.append('pass', pass);
    formData.append('avatar', avatar);

    if ((name === '') ) {
        $('input[name="name"]').addClass("is-invalid");
        return false;
    } 
    if ((parent === '') ) {
        $('input[name="parent"]').addClass("is-invalid");
        return false;
    } 
    if (birthday === '') {
        $('input[name="birthday"]').addClass("is-invalid");
        return false;
        }
    if (num_group === '') {
        $('input[name="num_group"]').addClass("is-invalid");
        return false;
        }
    if (pass === '') {
        $('input[name="pass"]').addClass("is-invalid");
        return false;
        }

    $.ajax({
        url: 'create-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        data:  formData,

        success () { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            document.location.href = "table.php";
        }
    })
});

$('.create-pay-btn').click(function(e) {
    e.preventDefault(); 
   
    let id_kid = $('#select option:selected').val(); 
    let money = $('input[name="money"]').val();
    let date = $('input[name="date"]').val();


    
    // if ((money === '') ) {
    //     $('input[name="money"]').addClass("is-invalid");
    //     return false;
    // } 
    // if (date === '') {
    //     $('input[name="date"]').addClass("is-invalid");
    //     return false;
    //     }
    
    $.ajax({
        url: 'create-pay.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: { //Данные, которые будут отправлены на сервер (у нас - на файл create.php).
            id_kid: id_kid,
            money: money,
            date: date
        },

        success () { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            document.location.href = "table.php";
        }
    })
});