/* 
    Валидация для формы редактирования
*/
$('input[name="update_avatar"]').change(function (e) {
    avatar = e.target.files[0];
    $("#img-to-edit").attr("style", "display:none");
    $("#update_avatar").attr("style", "");
});
$('#delete_checkbox').click(function(e){
    if (this.checked) //
       { 
           $("#img-to-edit").attr("style", "display:none");
           $("#update_avatar").attr("style", "display:none");
       }
    else {
            $("#img-to-edit").attr("style", "");
       }
});
$("#img-to-edit").click(function () {
    $("#update_avatar").trigger('click');
});


$('.update-btn').click(function(e) {
    e.preventDefault(); // отменяет действие браузера по умолчанию
    let id = $('input[name="id"]').val(); 
    let name = $('input[name="name"]').val(); 
    let parent = $('input[name="parent"]').val();
    let birthday = $('input[name="birthday"]').val();
    let num_group = $('input[name="num_group"]').val();
    let pass = $('input[name="pass"]').val();
    
    let formData = new FormData();
    formData.append('id', id);
    formData.append('name', name);
    formData.append('parent', parent);
    formData.append('birthday', birthday);
    formData.append('num_group', num_group);
    formData.append('pass', pass);
    formData.append('delete', delete_avatar);
    formData.append('avatar', avatar);
    
    if ((name === '') || (name.length<8)) {
        $('input[name="name"]').addClass("is-invalid");
        return false;
    } 
    if ((parent === '') || (parent.length<8)) {
        $('input[name="name"]').addClass("is-invalid");
        return false;
    } 
    if (birthday === '') {
        $('input[name="birthday"]').addClass("is-invalid");
        return false;
        }
    if (num_group === '') {
        $('input[name="num_group"]').addClass("is-invalid");
        return false;
        }
    if (pass === '') {
        $('input[name="pass"]').addClass("is-invalid");
        return false;
        }

    $.ajax({
        url: 'update-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,

        success (result) { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            if (result.status) { 
            document.location.href = "table.php";
            }
        }
    })
});

/* 
    Валидация для формы редактирования
*/

$('.update-pay-btn').click(function(e) {
    e.preventDefault(); // отменяет действие браузера по умолчанию
    let id = $('input[name="id"]').val(); 
    let id_kid = $('#select option:selected').val();
    let money = $('input[name="money"]').val(); 
    let date = $('input[name="date"]').val();
    
    
    /* if (money === '') {
        $('input[name="birthday"]').addClass("is-invalid");
        return false;
        }
    if (date === '') {
        $('input[name="num_group"]').addClass("is-invalid");
        return false;
        } */

    $.ajax({
        url: 'update-payment.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: { //Данные, которые будут отправлены на сервер (у нас - на файл create.php).
            id: id,
            id_kid: id_kid,
            money: money,
            date: date,
        },

        success () { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            document.location.href = "table.php";
        }
    })
});