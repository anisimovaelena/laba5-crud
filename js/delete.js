$('.del-btn').click(function(e) {
    e.preventDefault(); 

    $.ajax({
        url: 'delete-general.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: { //Данные, которые будут отправлены на сервер 
            id: id
        },

        success (result) { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            if (result.status) {
                document.location.href = "table-general.php";
            }
        }
    })
});

$('.del-pay-btn').click(function(e) {
    e.preventDefault(); 
    
    $.ajax({
        url: 'delete-pay.php', // страница где обрабатываются данные
        type: 'POST',
        dataType: 'json',
        data: { //Данные, которые будут отправлены на сервер 
            id: id
        },

        success (result) { // result - данные, которые возращает checkAuth.php (здесь - текст ошибки)
            if (result.status) {
                document.location.href = "table-general.php";
            }
        }
    })
});